package tpsa.ChessClient.Players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Stack;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tpsa.ChessClient.ChessClient;
import tpsa.ChessClient.Game.Board;
import tpsa.ChessClient.Game.BoardStateModificationException;
import tpsa.ChessClient.Game.Game;
import tpsa.ChessClient.Game.GameItem;
import tpsa.ChessClient.Game.GameMove;

public class ComputerPlayer extends Player {

	protected double centerWage = 0.05;
	static private Logger logger = Logger.getLogger(ComputerPlayer.class);
	static private Logger logVerbose = Logger
			.getLogger("Computer player verbose");
	protected double symmetryWage = 0.1;
	protected double exchangeWage = 0.1;
	protected Comparator<? super GameMove> comparator = new Comparator<GameMove>() {

		@Override
		public int compare(GameMove o1, GameMove o2) {
			// TODO Auto-generated method stub
			if (o2.value == o1.value)
				return 0;
			if (o2.value > o1.value)
				return 1;
			return -1;
		}
	};

	public double heuristicFunction(Board bo, PlayerType t) {

		double counterItems = 0;
		double counterExchange = 0;
		double center = 0.0;
		double centeredMine = 0.0;
		double centeredOpponent = 0.0;
		int mines = 0;
		int opponents = 0;

		// if (verboseLogger.getLevel() != Level.OFF) {
		// StringBuilder sb = new StringBuilder();
		// sb.append("Heurisitic function: ");
		// sb.append(", board: ");
		// sb.append(bo);
		// sb.append(", ");
		// sb.append(t);
		// verboseLogger.debug(sb);
		// }

		// FIXME dopracuj heurystyki

		for (int i = 0; i < Board.BoardSize; i++)
			for (int j = 0; j < Board.BoardSize; j++) {
				GameItem item = bo.items[i][j];
				if (GameItem.colors.get(item) == PlayerType.NONE)
					continue;
				double value = GameItem.values.get(item);
				if (GameItem.colors.get(item) == t) {
					counterItems += value;
					++mines;
					centeredMine += Math.abs((Board.BoardSize - 1) / 2.0 - j);
					if (j >= 2 && j <= 5 && i >= 3 && i <= 4) {
						center += 1.0f;
					}
				} else {
					++opponents;
					centeredOpponent += Math.abs((Board.BoardSize - 1) / 2.0
							- j);
					counterItems -= value;
					if (j >= 2 && j <= 5 && i >= 3 && i <= 4) {
						center -= 1.0f;
					}
				}

				if (GameItem.pawns.get(item)) {
					double increment = (7.0 - Math.abs(GameItem.colors
							.get(item).exchangeLine() - (double) i)) / 7.0;
					if (GameItem.colors.get(item) == t) {
						counterExchange += increment;
					} else {
						counterExchange -= increment;
					}
				}

			}

		centeredMine /= (Board.BoardSize - 1.0) / 2.0;
		centeredOpponent /= (Board.BoardSize - 1.0) / 2.0;
		centeredMine /= mines;
		centeredOpponent /= opponents;

		double result = counterItems + center * centerWage + counterExchange
				* exchangeWage + (centeredOpponent - centeredMine)
				* symmetryWage;

		// if (verboseLogger.getLevel() != Level.OFF)
		// verboseLogger.debug(String.format(
		// "Result of heuristic function: %f", result));

		return result;

	}

	static {
		logger.setLevel(Level.INFO);
		logVerbose.setLevel(Level.OFF);
	}

	public ComputerPlayer(PlayerType color, String name) {
		super(color, name);
	}

	public ComputerPlayer(PlayerType color, String name, int depth) {
		super(color, name);
		this.setDepth(depth);
	}

	private double alphabeta(Board bo, int depth, double alfa, double beta,
			PlayerType t, Stack<GameMove> movesHistory)
			throws BoardStateModificationException, InterruptedException {

		ArrayList<GameMove> moves = bo.generateMoves(t);
		ArrayList<GameMove> correctMoves = new ArrayList<GameMove>();

		boolean killing = false;

		for (GameMove move : moves) {
			boolean legal = bo.checkLegalMove(move, false);
			if (!legal)
				continue;

			boolean isKilling = move.type.isKilling();

			if (killing) {
				if (!isKilling)
					continue;
			} else {
				if (isKilling) {
					correctMoves.clear();
					killing = true;
				}
			}

			// teraz tu oceniać trzeba

			Board bNext = (Board) bo.clone();
			bNext.performMove(move);
			move.value = heuristicFunction(bNext, t) + (Math.random() - 0.5)
					* 0.1;

			ChessClient.pool.release(bNext);

			correctMoves.add(move);

		}

		if (correctMoves.size() <= 0) {
			// ruch terminalny
			return Board.LOSE - depth;
		}

		if (depth <= 0 && t.equals(type)) {
			// horyzont obliczeń
			return heuristicFunction(bo, t);
		}

		// wchodzimy w rekursję

		Collections.sort(correctMoves, comparator);

		for (GameMove move : correctMoves) {
			if (Thread.currentThread().isInterrupted()) {
				logger.warn("Current thread: "
						+ Thread.currentThread().getName() + " interrupted");
				throw new InterruptedException("On user request");
			}
			Board bNext = (Board) bo.clone(); // TODO remove dwa razy to samo
												// klonowanie
			bNext.performMove(move);
			PlayerType tNext = (move.type.isKilling()
					&& bNext.generateKillings(move).size() > 0 && !move.exchanged) ? t
					: t.next();

			movesHistory.push(move);
			double val = Double.NaN;
			if (!tNext.equals(t))
				val = -alphabeta(bNext, depth - 1, -beta, -alfa, tNext,
						movesHistory);
			else
				val = alphabeta(bNext, depth - 1, alfa, beta, tNext,
						movesHistory);
			movesHistory.pop();

			ChessClient.pool.release(bNext);

			if (Double.isNaN(val))
				return Double.NaN;

			if (val >= beta)
				return val;
			if (val > alfa)
				alfa = val;

		}
		return alfa;

	}

	@Override
	public GameMove performMove(Game g, GameMove lastKilling)
			throws BoardStateModificationException, InterruptedException {
		Stack<GameMove> moves = new Stack<GameMove>();
		GameMove bestMove = null;

		ArrayList<GameMove> correctMoves;
		correctMoves = (lastKilling == null) ? g.generateMoves(type) : g
				.generateKillings(lastKilling);

		System.out.println("Mój color: " + type);

		for (GameMove move : correctMoves) {
			if (Thread.interrupted())
				return null;
			Board gNext = g.getBoard();
			gNext.performMove(move);
			move.value = heuristicFunction(gNext, type);
			ChessClient.pool.release(gNext);
		}

		// logger.debug(String.format("Pierwszy: %.2f",
		// correctMoves.get(0).value));
		// logger.debug(String.format("Ostatni: %.2f",
		// correctMoves.get(correctMoves.size() - 1).value));

		// for (int depth = 2; depth <= ChessClient.depthComputer; depth +=
		// 2) {

		Collections.sort(correctMoves,comparator);
		logger.info("Calculating at depth " + getDepth());

		for (GameMove move : correctMoves) {
			/*
			 * Pierwszy lepszy ruch
			 */
			if (Thread.interrupted())
				return null;

			if (bestMove != null && bestMove.type.isKilling()
					&& !move.type.isKilling())
				continue; // i tak musimy zabić, bo to wynika z
							// regulaminu
			//  i tak nie możemy innego wykonać posunięcia

			Board gNext = g.getBoard();
			gNext.performMove(move);

			moves.push(move);

			ArrayList<GameMove> potentialKillings = gNext
					.generateKillings(move);

			PlayerType nextType = (move.type.isKilling()
					&& potentialKillings.size() > 0 && !move.exchanged) ? type
					: type.next();

			// logger.debug("Ruch dla " + nextType + ", ruchy: " +
			// moves);

			// logger.info("Ruch precalcualte: " + move.toString()
			// + ", alfa: " + move.alfa + ", beta: " + move.beta);

			double beta = 11000;
			double alfa = -11000;

			double val = Double.NaN;

			if (!nextType.equals(type))
				val = -alphabeta(gNext, getDepth() - 1, -beta, -alfa, nextType,
						moves);
			else
				val = alphabeta(gNext, getDepth(), alfa, beta, nextType, moves);

			if (Double.isNaN(val))
				return null;

			move.value = val;

			moves.pop();

			logger.info("Ruch calculated: " + move.toString() + ", value: "
					+ move.value);
			// logger.debug("Color1: " + g.getColor(move.from.x,
			// move.from.y));
			// logger.debug("Color2: " + g.getColor(move.to.x,
			// move.to.y));

			if (bestMove == null
					|| (!bestMove.type.isKilling() && move.type.isKilling())
					|| bestMove.value <= move.value) {
				logger.debug("Ten ruch był lepszy od " + bestMove);
				bestMove = move;
			}

			ChessClient.pool.release(gNext);
			// return move;
		}

		// }

		logger.info("Wybrano: " + bestMove);

		return bestMove;

	}

	@Override
	public String toString() {
		return "Computer player: " + getName() + ", " + type;
	}
}
