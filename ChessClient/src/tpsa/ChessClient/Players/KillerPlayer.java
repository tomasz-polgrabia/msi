package tpsa.ChessClient.Players;


public class KillerPlayer extends ComputerPlayer {

	public KillerPlayer(PlayerType color, String name) {
		super(color, name);
		
		comparator = new KillingComparator();
		
		exchangeWage = 1.0;
		
	}

	public KillerPlayer(PlayerType color, String name, int depth) {
		super(color, name, depth);
		comparator = new KillingComparator();
		exchangeWage = 1.0;
	}

}
