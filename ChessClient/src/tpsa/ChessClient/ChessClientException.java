package tpsa.ChessClient;

public class ChessClientException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1004009159560082887L;

	public ChessClientException() {
		super();
	}

	public ChessClientException(String msg) {
		super(msg);
	}

}
