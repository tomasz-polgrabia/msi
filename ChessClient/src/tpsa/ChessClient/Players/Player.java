package tpsa.ChessClient.Players;

import tpsa.ChessClient.Game.BoardStateModificationException;
import tpsa.ChessClient.Game.Game;
import tpsa.ChessClient.Game.GameMove;

public abstract class Player {

	private String name;
	public PlayerType type;
	private int depth = 8;

	public Player(PlayerType type, String name) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public abstract GameMove performMove(Game b, GameMove lastKilling)
			throws BoardStateModificationException, InterruptedException;

	public void setName(String name) {
		this.name = name;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

}
