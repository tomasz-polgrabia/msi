package tpsa.ChessClient.Game;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tpsa.ChessClient.Players.ComputerPlayer;
import tpsa.ChessClient.Players.HumanPlayer;
import tpsa.ChessClient.Players.Player;
import tpsa.ChessClient.Players.PlayerType;

public class Game {
	private Board board;
	private Player players[];

	private static Logger logger = Logger.getLogger(Game.class);

	static {
		logger.setLevel(Level.DEBUG);
	}

	public Game() {
		board = new Board();
		players = new Player[2];
		players[0] = new HumanPlayer(PlayerType.WHITE, "Tomek");
		// players[0] = new ComputerPlayer(PlayerType.WHITE, "Computer 1");
		players[1] = new ComputerPlayer(PlayerType.BLACK, "Computer 2");
	}

	public Game(Player p1, Player p2) {
		board = new Board();
		players = new Player[2];
		players[0] = p1;
		players[1] = p2;
	}

	public boolean checkLegal(GameMove move)
			throws BoardStateModificationException {
		return board.checkLegalMoveHuman(move);
	}

	@Override
	public Object clone() {
		Game g = new Game();
		g.board = (Board) board.clone();
		g.players = players.clone();
		return g;
	}

	public Game CloneAndPerform(GameMove move) {
		Game gClone = (Game) clone();
		try {
			gClone.perform(move);
		} catch (BoardStateModificationException e) {
			e.printStackTrace();
		}
		return gClone;
	}

	public int countPlayers() {
		return 2;
	}

	public String dumpBoard() {
		return board.toString();
	}

	public ArrayList<GameMove> generateKillings(GameMove move) {
		ArrayList<GameMove> moves = new ArrayList<GameMove>();
		ArrayList<GameMove> correctMoves = new ArrayList<GameMove>();

		GameItem item = board.items[move.to.y][move.to.x];
		if (GameItem.pawns.get(item))
		{
			moves.addAll(generateKillingsForPawn(move));
			board.cleanMoves(moves, correctMoves);
			return correctMoves;
		}
		if (GameItem.queens.get(item)) {
			board.generateQueenMoves(move.to, GameItem.colors.get(item), moves);
			ArrayList<GameMove> kills = new ArrayList<GameMove>();
			for (GameMove m : moves)
				if (m.type.isKilling())
					kills.add(m);
			moves.clear();
			board.cleanMoves(kills, moves);
		}
		
		return moves;
	}

	public ArrayList<GameMove> generateKillingsForPawn(GameMove move) {
		ArrayList<GameMove> moves = new ArrayList<GameMove>();
		board.genKillingsForPawn(
				GameItem.colors.get(board.items[move.to.y][move.to.x]), moves,
				move.to);
		ArrayList<GameMove> correctMoves = new ArrayList<GameMove>();
		board.cleanMoves(moves, correctMoves);
		return correctMoves;
	}

	public ArrayList<GameMove> generateMoves(PlayerType t) {
		return board.generateMoves(t);
	}

	public Board getBoard() {
		return (Board) board.clone();
	}

	public FieldType getColor(int x, int y) {
		return ((x + y) % 2 == 0) ? FieldType.BLACK : FieldType.WHITE;
	}

	public GameItem GetItem(int x, int y) {
		return board.items[y][x];
	}

	public Player getPlayer(int i) {
		return players[i];
	}

	public PlayerType getTypeOfPlayer(Player p) {
		return p.type;
	}

	public Player next(Player p) {
		switch (p.type) {
		case WHITE:
			return players[0];
		case BLACK:
			return players[1];
		default:
			return null;
		}
	}

	public Game perform(GameMove move) throws BoardStateModificationException {
		board.performMove(move);
		return this;
	}

	public void RegisterPlayer(Player p, PlayerType type)
			throws BoardHasSuchTypeException {
		switch (type) {
		case WHITE:
			players[0] = p;
			break;
		case BLACK:
			players[1] = p;
			break;
		default:
			throw new BoardHasSuchTypeException();
		}
	}

	public void UnregisterPlayer(Player p) {

	}

}
