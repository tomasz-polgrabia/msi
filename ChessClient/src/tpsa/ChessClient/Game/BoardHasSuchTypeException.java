package tpsa.ChessClient.Game;

import tpsa.ChessClient.ChessClientException;

public class BoardHasSuchTypeException extends ChessClientException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1910858347778762415L;

	public BoardHasSuchTypeException() {
		super();
	}

	public BoardHasSuchTypeException(String msg) {
		super(msg);
	}

}
