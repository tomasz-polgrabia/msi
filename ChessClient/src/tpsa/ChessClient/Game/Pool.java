package tpsa.ChessClient.Game;

import java.util.LinkedList;

public class Pool {
	private LinkedList<Board> items;
	private final int count = 666;

	public Pool(int start) {
		items = new LinkedList<Board>();
		generateNewBoards(start);
	}

	private void generateNewBoards(int start) {
		for (int i = 0; i < start; i++)
			items.add(new Board());
	}

	synchronized public Board get() {
		if (items.size() <= 0)
			generateNewBoards(count);
		return items.remove();
	}

	synchronized public void release(Board b) {
		items.add(b);
	}

}
