package tpsa.ChessClient.Players;

import tpsa.ChessClient.Game.Game;
import tpsa.ChessClient.Game.GameMove;

public class HumanPlayer extends Player {

	public HumanPlayer(PlayerType type, String name) {
		super(type, name);
	}

	@Override
	public GameMove performMove(Game b, GameMove lastKilling) {
		return null;

	}

}
