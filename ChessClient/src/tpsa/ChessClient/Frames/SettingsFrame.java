package tpsa.ChessClient.Frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tpsa.ChessClient.ChessClient;
import tpsa.ChessClient.Players.ComputerPlayer;
import tpsa.ChessClient.Players.Player;

public class SettingsFrame extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1117702673985379222L;
	private static Logger logger = Logger.getLogger(SettingsFrame.class);

	static {
		logger.setLevel(Level.WARN);
	}

	private JTextField pDepth1;
	private JTextField pName1;
	private JCheckBox c1;
	private JTextField pDepth2;
	private JTextField pName2;
	private JCheckBox c2;

	public SettingsFrame(Player p1, Player p2) throws HeadlessException {

		super();
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Computer Ai settings");
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.anchor = GridBagConstraints.CENTER;

		final JLabel lDepth1 = new JLabel("Głębokość namysłu: ");
		pDepth1 = new JTextField(Integer.toString(p1.getDepth()), 5);
		final JLabel lName1 = new JLabel("Nazwa: ");
		pName1 = new JTextField(p1.getName(), 5);
		c1 = new JCheckBox();
		c1.setSelected(p1 instanceof ComputerPlayer);
		AbstractAction change1 = new AbstractAction("Komputer") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5578242470162589639L;

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean show = c1.isSelected();
//				lName1.setEnabled(show);
//				pName1.setEnabled(show);
				lDepth1.setEnabled(show);
				pDepth1.setEnabled(show);
			}
		};

		c1.setAction(change1);

		final JLabel lDepth2 = new JLabel("Głębokość namysłu: ");
		pDepth2 = new JTextField(Integer.toString(p2.getDepth()), 5);
		final JLabel lName2 = new JLabel("Nazwa: ");
		pName2 = new JTextField(p2.getName(), 5);
		c2 = new JCheckBox();
		c2.setSelected(p2 instanceof ComputerPlayer);
		AbstractAction change2 = new AbstractAction("Komputer") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5578242470162589639L;

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean show = c2.isSelected();
//				lName2.setEnabled(show);
//				pName2.setEnabled(show);
				lDepth2.setEnabled(show);
				pDepth2.setEnabled(show);
			}
		};

		c2.setAction(change2);

		c.ipadx = 5;
		c.ipady = 5;
		c.weightx = 1;
		// c.fill = GridBagConstraints.HORIZONTAL;

		c.gridy = 0;

		panel.add(c1, c);

		c.gridx = 1;

		c.fill = GridBagConstraints.HORIZONTAL;
		panel.add(lDepth1, c);

		c.gridx = 2;
		c.fill = GridBagConstraints.NONE;
		panel.add(pDepth1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;

		panel.add(lName1, c);
		c.gridx = 5;
		c.fill = GridBagConstraints.NONE;
		panel.add(pName1, c);

		c.gridy = 1;

		c.gridx = 0;
		panel.add(c2, c);

		c.gridx = 1;

		c.fill = GridBagConstraints.HORIZONTAL;
		panel.add(lDepth2, c);

		c.gridx = 2;
		c.fill = GridBagConstraints.NONE;
		panel.add(pDepth2, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;

		panel.add(lName2, c);
		c.gridx = 5;
		c.fill = GridBagConstraints.NONE;
		panel.add(pName2, c);

		c.fill = GridBagConstraints.NONE;

		c.gridy = 2;
		c.gridx = 0;

		c.gridwidth = 6;
		panel.add(new JButton(new AbstractAction("OK") {

			/**
			 * s
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int value = Integer.parseInt(pDepth1.getText());
					if (value < 0) {
						logger.warn("Musi być dodatnia");
						JOptionPane.showMessageDialog(getRootPane(),
								"Musi być dodatnia");
						return;
					}
					ChessClient.depthComputer = value;
				} catch (NumberFormatException excp) {
					JOptionPane.showMessageDialog(getRootPane(),
							excp.getMessage());
					logger.error("setting depth calc ", excp);
				}

				setVisible(false);
			}
		}), c);

		setPreferredSize(new Dimension(600, 400));
		add(panel, BorderLayout.CENTER);
		pack();

	}

	public int getTypeFirst() {
		return c1.isSelected() ? 1 : 0;
	}

	public int getTypeSecond() {
		return c2.isSelected() ? 1 : 0;
	}

	public int getDepthFirst() {
		try {
			return Integer.parseInt(pDepth1.getText());
		} catch (NumberFormatException e) {
			return ChessClient.depthComputer;
		}
	}

	public int getDepthSecond() {
		try {
			return Integer.parseInt(pDepth2.getText());
		} catch (NumberFormatException e) {
			return ChessClient.depthComputer;
		}
	}
	
	public String getNameFirst()
	{
		return pName1.getText();
	}
	
	public String getNameSecond()
	{
		return pName2.getText();
	}

}
