package tpsa.ChessClient.Game;

import java.awt.Point;

import tpsa.ChessClient.Players.PlayerType;

public class GameMove {

	public Point from;
	public Point to;
	public GameMoveType type;
	public double value;
	public String msg;
	public PlayerType color;
	public int id;
	public boolean exchanged = false;

	public GameMove(Point from, Point to) {
		this.from = from;
		this.to = to;
		this.value = 0;
		this.type = GameMoveType.NONE;
	}

	public GameMove(Point from, Point to, double value) {
		this.from = from;
		this.to = to;
		this.value = value;
		this.type = GameMoveType.NONE;
	}

	@Override
	public String toString() {
		int idx1 = from.y + 1;
		int idx2 = to.y + 1;
		String separator = type.isKilling() ? ":" : "-";
		return "" + id + ". " + (char) (from.x + 'a') + "" + idx1 + separator + (char) (to.x + 'a')
				+ idx2;
	}

}
