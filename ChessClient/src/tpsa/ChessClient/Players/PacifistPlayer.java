package tpsa.ChessClient.Players;

public class PacifistPlayer extends ComputerPlayer {

	public PacifistPlayer(PlayerType color, String name) {
		super(color, name);
		comparator = new NotKillerComparator();
	}

	public PacifistPlayer(PlayerType color, String name, int depth) {
		super(color, name, depth);
		comparator = new NotKillerComparator();
	}

}
