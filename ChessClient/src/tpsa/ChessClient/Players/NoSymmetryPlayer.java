package tpsa.ChessClient.Players;

public class NoSymmetryPlayer extends ComputerPlayer {

	public NoSymmetryPlayer(PlayerType color, String name) {
		super(color, name);
		symmetryWage = 0;
	}

	public NoSymmetryPlayer(PlayerType color, String name, int depth) {
		super(color, name, depth);
		symmetryWage = 0;
	}

}
