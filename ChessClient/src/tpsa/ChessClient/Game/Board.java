package tpsa.ChessClient.Game;

import java.awt.Point;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tpsa.ChessClient.ChessClient;
import tpsa.ChessClient.Players.PlayerType;

// FIXME damki mogą bić tylko jedną bierkę naraz

// FIXME po promocję na hetmana (z biciem) hetman wykonuje 2x ruch pod rząd

public class Board {

	public final static int BoardSize = 8;
	public static final double WIN = 10000;
	public static final double LOSE = -WIN;
	private static Logger logger = Logger.getLogger(Board.class);
	// private static Logger verboseLogger = Logger
	// .getLogger("Board verbose logger");
	static {
		logger.setLevel(Level.DEBUG);
		// verboseLogger.setLevel(Level.OFF);
	}

	static public double exchangeLine(Board bo, PlayerType t) {
		double value = 0.0f;
		for (int i = 0; i < Board.BoardSize; i++)
			for (int j = 0; j < Board.BoardSize; j++) {
				GameItem item = bo.items[i][j];
				if (item == GameItem.EMPTY)
					continue;
				if (GameItem.pawns.get(item)) {
					double increment = (7.0 - Math.abs(GameItem.colors
							.get(item).exchangeLine() - (double) i)) / 7.0;
					if (GameItem.colors.get(item) == t)
						value += increment;
					else
						value -= increment;
				}
			}

		return value * 0.01;
	}

	static private int sign(int x) {
		if (x > 0)
			return 1;
		if (x < 0)
			return -1;
		return 0;
	}

	public GameItem[][] items;

	public Board() {
		items = new GameItem[BoardSize][BoardSize];

		for (int i = 0; i < BoardSize; i++)
			for (int j = 0; j < BoardSize; j++)
				items[i][j] = GameItem.EMPTY;

		for (int i = 0; i < 3; i++)
			for (int j = 0; j < BoardSize; j++) {
				if ((i + j) % 2 == 0)
					items[i][j] = GameItem.WHITE_PAWN;
			}

		for (int i = BoardSize - 3; i < BoardSize; i++)
			for (int j = 0; j < BoardSize; j++) {
				if ((i + j) % 2 == 0)
					items[i][j] = GameItem.BLACK_PAWN;
			}

		// items[7][7] = GameItem.WHITE_QUEEN;
		// items[2][2] = GameItem.BLACK_PAWN;
		// items[1][1] = GameItem.BLACK_PAWN;

		// items[6][6] = GameItem.WHITE_QUEEN;
		// items[2][2] = GameItem.BLACK_PAWN;
		// items[1][1] = GameItem.BLACK_PAWN;

		// dziwne dlaczego nie pójdzie na drugą damkę FIXME
		// items[5][5] = GameItem.WHITE_QUEEN;
		// items[2][2] = GameItem.BLACK_PAWN;
		// items[1][1] = GameItem.BLACK_PAWN;

		// items[1][1] = GameItem.BLACK_QUEEN;
		// items[0][2] = GameItem.WHITE_QUEEN;

		// items[2][2] = GameItem.WHITE_PAWN;
		// items[4][4] = GameItem.WHITE_PAWN;
		// items[7][7] = GameItem.BLACK_QUEEN;
		// items[3][7] = GameItem.WHITE_PAWN;
		// items[2][6] = GameItem.BLACK_PAWN;

		// items[0][0] = GameItem.WHITE_PAWN;
		// items[4][0] = GameItem.WHITE_PAWN;
		// items[3][1] = GameItem.WHITE_QUEEN;
		// items[3][7] = GameItem.WHITE_PAWN;
		//
		// items[2][4] = GameItem.BLACK_PAWN;
		// items[1][7] = GameItem.BLACK_QUEEN;

		// test

		// items[6][6] = GameItem.WHITE_QUEEN;
		// items[5][7] = GameItem.WHITE_PAWN;
		//
		// items[7][1] = GameItem.BLACK_PAWN;

		// test

		// items[7][7] = GameItem.BLACK_QUEEN;
		// items[0][0] = GameItem.WHITE_PAWN;
		// items[1][1] = GameItem.WHITE_PAWN;

		// test

		// items[7][1] = GameItem.BLACK_QUEEN;
		// items[1][7] = GameItem.WHITE_PAWN;
		// items[2][6] = GameItem.WHITE_PAWN;

		// test

		// items[7][7] = GameItem.WHITE_PAWN;
		// items[4][4] = GameItem.WHITE_PAWN;
		// items[0][0] = GameItem.BLACK_QUEEN;

		// test

		// items[7][7] = GameItem.BLACK_QUEEN;
		// items[3][3] = GameItem.WHITE_PAWN;
		// items[0][2] = GameItem.WHITE_PAWN;

		// test

		// items[0][0] = GameItem.WHITE_QUEEN;
		// items[0][2] = GameItem.WHITE_PAWN;
		// items[7][5] = GameItem.BLACK_QUEEN;
		// items[7][3] = GameItem.BLACK_QUEEN;

	}

	public Board(Board bo) {
		items = new GameItem[Board.BoardSize][Board.BoardSize];
		copyBoard(bo);
	}

	private boolean checkCorrectPawnKilling(GameMove move, boolean description) {
		int diffX = Math.abs(move.to.x - move.from.x);
		int diffY = Math.abs(move.to.y - move.from.y);

		if (diffX != diffY)
			return false;

		if (diffX <= 1) {
			// verboseLogger.debug("Zwykły ruch");
			move.type = GameMoveType.NORMAL;
			return true;
		}

		if (diffX > 2 || diffY > 2)
			return false;

		int middleX = (move.from.x + move.to.x) / 2;
		int middleY = (move.from.y + move.to.y) / 2;
		Point from = move.from;

		GameItem fromItem = items[from.y][from.x];
		GameItem middleItem = items[middleY][middleX];

		if (fromItem == null || middleItem == null) {
			if (description)
				move.msg = String.format("from Item or middle Item is null");
			return false;
		}

		if (middleItem == GameItem.EMPTY) {
			if (description)
				move.msg = String.format("middle item is empty");
			return false;
		}

		// TODO TEST
		if (GameItem.colors.get(fromItem).equals(
				GameItem.colors.get(middleItem))) {
			if (description)
				move.msg = String.format("Killing the same color");
			return false;
		}

		// verboseLogger.debug("Poprawne bicie");

		move.type = GameMoveType.KILLING_FRONT;

		// różne biją się
		return true;

	}

	private boolean checkForKillingBehind(GameMove move, boolean description) {

		int diffX = Math.abs(move.to.x - move.from.x);
		int diffY = Math.abs(move.to.y - move.from.y);

		// wiemy, że move.to ruch do tyłu

		if (diffX != diffY || diffX != 2) {
			if (description)
				move.msg = String.format(
						"diffx %d and diffy %d are different or not killing",
						diffX, diffY);
			return false;
		}

		Point middle = new Point(0, 0);
		middle.x = (move.from.x + move.to.x) / 2;
		middle.y = (move.from.y + move.to.y) / 2;

		GameItem middleItem = items[middle.y][middle.x];
		Point from = move.from;

		if (middleItem == GameItem.EMPTY
				|| GameItem.colors.get(middleItem).equals(
						GameItem.colors.get(items[from.y][from.x]))) {
			if (description)
				move.msg = String.format(
						"Middle item %s is empty or the same of killing",
						middleItem);
			return false;
		}

		// logger.debug("Bicie do tyłu");
		move.type = GameMoveType.KILLING_BACK;
		return true;
	}

	public boolean checkLegalMove(GameMove move, boolean description) {
		if ((((move.to.x + move.to.y) % 2 == 0) ? FieldType.BLACK
				: FieldType.WHITE) == FieldType.WHITE) {
			move.msg = "You cannot move on white field";
			return false;
		}
		if (!checkPointsCoordinates(move.from)) {
			if (description)
				move.msg = "Uncorrect coordinates : " + move.from.toString();
			return false;
		}

		if (!checkPointsCoordinates(move.to)) {
			if (description)
				move.msg = "Uncorrect coordinates : " + move.to.toString();
			return false;
		}

		if ((move.to.x + move.to.y) % 2 == 1) {
			return false;
		}

		GameItem itemFrom = items[move.from.y][move.from.x];
		GameItem itemTo = items[move.to.y][move.to.x];

		if (itemFrom == null || itemTo == null) {
			if (description)
				move.msg = "Items from or to are null";
			return false;
		}

		if (itemFrom == GameItem.EMPTY) {
			if (description)
				move.msg = "Item from is empty";
			return false;
		}

		if (!(itemTo == GameItem.EMPTY)) {
			if (description)
				move.msg = "Items to is empty";
			return false;
		}

		int diffY = move.to.y - move.from.y;

		switch (itemFrom) {
		case WHITE_PAWN:
			if (sign(diffY) == -PlayerType.WHITE.getDirectionMove())
				if (!checkForKillingBehind(move, description))
					return false;
			if (!checkPawn(move, description))
				return false;
			break;
		case BLACK_PAWN:
			// verboseLogger.debug("Bicie do tyłu");
			if (sign(diffY) == -PlayerType.BLACK.getDirectionMove())
				if (!checkForKillingBehind(move, description))
					return false;
			if (!checkPawn(move, description))
				return false;
			break;
		case BLACK_QUEEN:
			if (!checkQueen(move))
				return false;
			break;
		case EMPTY:
			break;
		case WHITE_QUEEN:
			if (!checkQueen(move))
				return false;
			break;
		default:
			break;
		}

		return true;

	}

	public boolean checkLegalMoveHuman(GameMove move)
			throws BoardStateModificationException {
		if (!checkLegalMove(move, true)) {
			if (move.msg == null)
				move.msg = "";
			BoardStateModificationException excp = new BoardStateModificationException(
					move.msg);
			throw excp;
		}

		GameItem item = items[move.from.y][move.from.x];

		ArrayList<Point> pawns = new ArrayList<Point>();
		ArrayList<Point> queens = new ArrayList<Point>();
		generateAllItems(pawns, queens, GameItem.colors.get(item));
		ArrayList<GameMove> moves = new ArrayList<GameMove>();
		// TODO generateListQueens(item.getType());

		genPawnKillings(pawns, GameItem.colors.get(item), moves);
		cleanMoves(moves);
		generateQueensMoves(queens, GameItem.colors.get(item), moves);
		ArrayList<GameMove> killMoves = new ArrayList<GameMove>();
		for (GameMove m2 : moves)
			if (m2.type.isKilling())
				killMoves.add(m2);

		// checkLegalMove(move);

		// TODO genQu

		if (killMoves.size() > 0 && !move.type.isKilling()) {
			BoardStateModificationException excp = new BoardStateModificationException(
					String.format(
							"Possible killings available, not executed: %s",
							moves));
			throw excp;
		}

		return true;
	}

	private boolean checkPawn(GameMove move, boolean description) {
		int diffX = move.to.x - move.from.x;
		int diffY = move.to.y - move.from.y;

		if (Math.abs(diffX) != Math.abs(diffY)) // musi symetrycznie
		{
			if (description)
				move.msg = String.format(
						"DiffX - %d and DiffY - %d must be symmetric", diffX,
						diffY);
			return false;
		}

		if (Math.abs(diffY) > 2) // o tyle nie może się ruszać
		{
			if (description)
				move.msg = String.format("Too long move for pawn - %d", diffY);
			return false;
		}

		// tu nie sprawdzamy czy jest poprawne bicie

		if (Math.abs(diffX) <= 1)
			return true;

		if (checkCorrectPawnKilling(move, description))
			return true;
		return false;
	}

	private boolean checkPointsCoordinates(Point p) {
		return (p.x < 0 || p.x >= BoardSize || p.y < 0 || p.y >= BoardSize) ? false
				: true;
	}

	private boolean checkQueen(GameMove move) {
		int diffX = move.to.x - move.from.x;
		int diffY = move.to.y - move.from.y;

		if (Math.abs(diffX) != Math.abs(diffY))
			return false;

		// sprawdzamy czy jest może bicie i wtedy czy poprawne bicie

		int perXMove = 1;
		int perYMove = 1;

		if (diffX < 0)
			perXMove = -1;

		if (diffY < 0)
			perYMove = -1;

		int count = Math.abs(diffX);
		int posX = move.from.x;
		int posY = move.from.y;

		GameItem fromItem = items[move.from.y][move.from.x];

		boolean bicie = false;
		for (int i = 0; i < count - 1; i++) {
			posX += perXMove;
			posY += perYMove;
			GameItem currentField = items[posY][posX];
			if (GameItem.colors.get(currentField) == GameItem.colors
					.get(fromItem))
				return false;
			// nie można skakać przez swoich
			if (currentField != GameItem.EMPTY) {
				if (currentField != fromItem) {
					if (!bicie)
						bicie = true;
					else
						return false;
				}
			} else {
				// if (bicie)
				// return false;
			}

		}

		if (bicie)
			move.type = GameMoveType.KILLING_QUEEN;
		return true;

	}

	private void cleanMoves(ArrayList<GameMove> moves) {
		ArrayList<GameMove> cleanMoves = new ArrayList<GameMove>();
		cleanMoves(moves, cleanMoves);

		moves.clear();
		moves.addAll(cleanMoves);

	}

	public void cleanMoves(ArrayList<GameMove> moves,
			ArrayList<GameMove> correctMoves) {
		for (GameMove move : moves) {
			if (checkLegalMove(move, false))
				correctMoves.add(move);
		}
	}

	@Override
	public Object clone() {
		// Board b = new Board(this);
		Board b = ChessClient.pool.get(); // getting from pool to be released
		b.copyBoard(this); // kopiujemy
		return b;
	}

	private void copyBoard(Board bo) {
		for (int i = 0; i < Board.BoardSize; i++)
			for (int j = 0; j < Board.BoardSize; j++)
				items[j][i] = bo.items[j][i];
	}

	private boolean deleteAllBetween(Point from, Point to) {
		// nie sprawdzamy tu czy jest na ukośnej i innych rzeczy
		// zakładamy, że dane są poprawne

		int diffX = to.x - from.x;
		int diffY = to.y - from.y;

		int progressX = 1;
		int progressY = 1;

		if (diffX < 0)
			progressX = -1;

		if (diffY < 0)
			progressY = -1;

		int posX = from.x + progressX;
		int posY = from.y + progressY;

		int count = Math.abs(diffX);

		for (int i = 0; i < count - 1; i++) {

			items[posY][posX] = GameItem.EMPTY;

			posX += progressX;
			posY += progressY;
		}

		return true;

	}

	public void generateAllItems(ArrayList<Point> pawns,
			ArrayList<Point> queens, PlayerType type) {
		for (int i = 0; i < Board.BoardSize; i++)
			for (int j = 0; j < Board.BoardSize; j++) {
				GameItem item = items[i][j];
				if (GameItem.colors.get(item) != type)
					continue;
				if (GameItem.pawns.get(item))
					pawns.add(new Point(j, i));
				else
					queens.add(new Point(j, i));
			}
	}

	public ArrayList<GameMove> gKillings(PlayerType t) {
		ArrayList<GameMove> moves = generateMoves(t);
		cleanMoves(moves);
		ArrayList<GameMove> killings = new ArrayList<GameMove>();
		for (GameMove m : moves)
			if (m.type.isKilling())
				killings.add(m);
		return killings;
	}

	public ArrayList<GameMove> generateKillings(GameMove move) {
		ArrayList<GameMove> moves = new ArrayList<GameMove>();

		GameItem item = items[move.to.y][move.to.x];
		if (GameItem.pawns.get(item))
			moves.addAll(generateKillingsForPawn(move));
		if (GameItem.queens.get(item)) {
			generateQueenMoves(move.to, GameItem.colors.get(item), moves);
			ArrayList<GameMove> kills = new ArrayList<GameMove>();
			for (GameMove m : moves)
				if (m.type.isKilling())
					kills.add(m);
			moves.clear();
			cleanMoves(kills, moves);
		}

		return moves;
	}

	public ArrayList<GameMove> generateKillingsForPawn(GameMove move) {
		ArrayList<GameMove> moves = new ArrayList<GameMove>();
		genKillingsForPawn(GameItem.colors.get(items[move.to.y][move.to.x]),
				moves, move.to);
		ArrayList<GameMove> correctMoves = new ArrayList<GameMove>();
		cleanMoves(moves, correctMoves);
		return correctMoves;
	}

	// private ArrayList<Point> generateListPawns(PlayerType type) {
	// ArrayList<Point> elems = new ArrayList<Point>();
	//
	// for (int i = 0; i < Board.BoardSize; i++)
	// for (int j = 0; j < Board.BoardSize; j++) {
	// GameItem item = items[i][j];
	// if (GameItem.colors.get(item).equals(type)
	// && GameItem.pawns.get(item))
	// elems.add(new Point(j, i));
	// }
	//
	// return elems;
	// }

	public ArrayList<GameMove> generateMoves(PlayerType t) {
		ArrayList<Point> pawns = new ArrayList<Point>();
		ArrayList<Point> queens = new ArrayList<Point>();

		PlayerType type = t;

		generateAllItems(pawns, queens, type);

		// generate all, maybe some not legal moves

		ArrayList<GameMove> moves = new ArrayList<GameMove>();
		ArrayList<GameMove> correctMoves = new ArrayList<GameMove>();

		genPawnNormMoves(pawns, type, moves);
		genPawnKillings(pawns, type, moves);

		// TODO generation moves for queens

		generateQueensMoves(queens, type, moves);

		cleanMoves(moves, correctMoves);

		moves.clear();
		return correctMoves;

	}

	public void generateQueenMoves(Point queen, PlayerType type,
			ArrayList<GameMove> moves) {
		int toLeft = queen.x;
		int toRight = BoardSize - queen.x - 1;
		int toUp = BoardSize - queen.y - 1;
		int toDown = queen.y;

		int step[][] = new int[][] { { 1, 1 }, { -1, 1 }, { 1, -1 }, { -1, -1 } };

		// góra prawo
		int count[] = new int[] { Math.min(toUp, toRight), // NR
				Math.min(toUp, toLeft), // NL
				Math.min(toDown, toRight), // SR
				Math.min(toDown, toLeft), // SL
		};

		ArrayList<GameMove> tempMoves = new ArrayList<GameMove>();

		for (int j = 0; j < count.length; j++) {

			Boolean killing = false;

			for (int i = 1; i <= count[j]; i++) {
				Point to = new Point(queen.x + step[j][0]*i, queen.y + step[j][1]*i);
				GameItem item = items[to.y][to.x];
				GameMove move = new GameMove(queen, to);
				if (GameItem.colors.get(item) == type.next()) {
					if (killing)
						break;
					killing = true;
					continue;
				}

				if (GameItem.colors.get(item) == type)
					break;

				tempMoves.add(move);

				if (killing && item == GameItem.EMPTY) 
					move.type = GameMoveType.KILLING_QUEEN;
				
			}

			moves.addAll(tempMoves);
			tempMoves.clear();
		}
	}

	public void generateQueensMoves(ArrayList<Point> queens, PlayerType type,
			ArrayList<GameMove> moves) {
		for (Point queen : queens)
			generateQueenMoves(queen, type, moves);
	}

	public void genKillingsForPawn(PlayerType type, ArrayList<GameMove> moves,
			Point p2) {
		// bicie do przodu na prawo
		Point to2 = new Point(p2.x + 2, p2.y + type.getDirectionMove() * 2);
		moves.add(new GameMove(p2, to2));

		// bicie do przodu na lewo
		to2 = new Point(p2.x - 2, p2.y + type.getDirectionMove() * 2);
		moves.add(new GameMove(p2, to2));

		// bicie do przodu na prawo
		to2 = new Point(p2.x + 2, p2.y + type.getDirectionMove() * (-2));
		moves.add(new GameMove(p2, to2));

		// bicie do przodu na lewo
		to2 = new Point(p2.x - 2, p2.y + type.getDirectionMove() * (-2));
		moves.add(new GameMove(p2, to2));
	}

	private void genPawnKillings(ArrayList<Point> pawns, PlayerType type,
			ArrayList<GameMove> moves) {
		for (Point p2 : pawns)
			genKillingsForPawn(type, moves, p2);
	}

	private void genPawnNormMoves(ArrayList<Point> pawns, PlayerType type,
			ArrayList<GameMove> moves) {
		for (Point p : pawns) {
			// zwykły ruch na prawo
			Point to1 = new Point(p.x + 1, p.y + type.getDirectionMove());
			moves.add(new GameMove(p, to1));

			// zwykły ruch na prawo
			to1 = new Point(p.x - 1, p.y + type.getDirectionMove());
			moves.add(new GameMove(p, to1));
		}
	}

	// public GameItem get(int x, int y) {
	// if (x < 0 || y < 0 || x >= BoardSize || y >= BoardSize)
	// return null;
	// GameItem elem = items[y][x];
	// // logger.debug(String.format("Biorę element z items (%d,%d) - %s", x,
	// // y,
	// // elem.toString()));
	// return elem;
	// }

	public boolean performMove(GameMove move)
			throws BoardStateModificationException {
		GameItem item = items[move.from.y][move.from.x];
		if (item == null)
			return false;

		if (GameItem.pawns.get(item)
				&& move.to.y == GameItem.colors.get(item).exchangeLine()) {
			if (GameItem.colors.get(item) == PlayerType.WHITE)
				item = GameItem.WHITE_QUEEN;
			else
				item = GameItem.BLACK_QUEEN;
			move.exchanged = true;
		}

		if (!deleteAllBetween(move.from, move.to))
			return false;
		if (!put(move.from.x, move.from.y, GameItem.EMPTY))
			return false;
		if (!put(move.to.x, move.to.y, item))
			return false;

		return true;

	}

	public boolean put(int x, int y, GameItem item) {

		if (x < 0 || y < 0 || x >= BoardSize || y >= BoardSize)
			return false;

		items[y][x] = item;
		return true;

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = Board.BoardSize - 1; i >= 0; i--) {
			for (int j = 0; j < Board.BoardSize; j++)
				sb.append(items[j][i] + ",");
			sb.append(System.getProperty("line.separator"));
		}

		return sb.toString();
	}

}
