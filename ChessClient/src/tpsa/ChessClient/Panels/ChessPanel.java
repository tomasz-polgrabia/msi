package tpsa.ChessClient.Panels;

// FIXME jak program nie widzi rozwizania - przegrana, to gra jak kamikadze

// FIXME na pewno executeComputer i pewnie executeHuman dopuszcza bicia 2x różnymi figurami (nie kontynuaacja 1.)
// co do alphabety nie wiem jak ta sytuacja stoi, dla 10 poziomu obydwa na samiutkim końcu
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tpsa.ChessClient.ChessClient;
import tpsa.ChessClient.ChessClientException;
import tpsa.ChessClient.Game.Board;
import tpsa.ChessClient.Game.BoardStateModificationException;
import tpsa.ChessClient.Game.FieldType;
import tpsa.ChessClient.Game.Game;
import tpsa.ChessClient.Game.GameItem;
import tpsa.ChessClient.Game.GameMove;
import tpsa.ChessClient.Players.ComputerPlayer;
import tpsa.ChessClient.Players.HumanPlayer;
import tpsa.ChessClient.Players.Player;
import tpsa.ChessClient.Players.PlayerType;

public class ChessPanel extends JPanel implements MouseListener,
		MouseMotionListener {

	private static Logger logger = Logger.getLogger(ChessPanel.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 8476421313191198782L;
	// private boolean locked = false;
	private boolean automatic = true;
	private DefaultListModel history;

	public DefaultListModel getHistory() {
		return history;
	}

	public void setHistory(DefaultListModel history) {
		this.history = history;
	}

	static {
		logger.setLevel(Level.DEBUG);
	}
	private Game game;
	private int xItemMoving = -1;
	private float xMouseDragging = -1;
	private float xMouseOffset = -1;
	private int yItemMoving = -1;
	private float yMouseDragging = -1;
	private int moveNr = 0;

	private float yMouseOffset = -1;
	private int current = 0;
	private JLabel statusLabel;

	public ChessPanel(DefaultListModel history, JLabel statusLabel) {
		this.history = history;
		this.statusLabel = statusLabel;
		setDoubleBuffered(true);
		game = new Game();
		addMouseListener(this);
		addMouseMotionListener(this);
		// Thread t = new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// Thread.sleep(1000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// executeNext();
		// }
		// });
		//
		// t.start();
	}

	public void executeNext() {
		current = (current + 1) % 2;
		executeCurrent();

	}

	public void executeCurrent() {

		logger.info("Executing : " + current);
		Player p = game.getPlayer(current);
		if (p instanceof HumanPlayer)
			executeHumanMove();
		else {
			executeComputer(p);
		}
	}

	private void drawItem(Graphics2D g2, int fieldWidth, int fieldHeight,
			GameItem item, int posX, int posY) {
		Shape gameObj1, gameObj2;
		switch (item) {
		case EMPTY:
			return;
		case WHITE_PAWN:
			gameObj1 = new Ellipse2D.Float(posX, posY, fieldWidth, fieldHeight);
			g2.setColor(Color.YELLOW);
			g2.fill(gameObj1);
			break;
		case WHITE_QUEEN:
			gameObj1 = new Ellipse2D.Float(posX, posY, fieldWidth, fieldHeight);
			g2.setColor(Color.YELLOW);
			g2.fill(gameObj1);
			gameObj2 = new Ellipse2D.Float(posX + 10, posY + 10,
					fieldWidth - 20, fieldHeight - 20);
			g2.setColor(Color.GREEN);
			g2.fill(gameObj2);
			break;
		case BLACK_PAWN:
			gameObj1 = new Ellipse2D.Float(posX, posY, fieldWidth, fieldHeight);
			g2.setColor(Color.RED);
			g2.fill(gameObj1);
			break;

		case BLACK_QUEEN:
			gameObj1 = new Ellipse2D.Float(posX, posY, fieldWidth, fieldHeight);
			g2.setColor(Color.RED);
			g2.fill(gameObj1);
			gameObj2 = new Ellipse2D.Float(posX + 10, posY + 10,
					fieldWidth - 20, fieldHeight - 20);
			g2.setColor(Color.GREEN);
			g2.fill(gameObj2);
			break;

		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		try {
			// TODO przesuwanie rzeczy
			Player player = game.getPlayer(current);
			if (player instanceof ComputerPlayer)
				return;
			// if (locked)
			// return;
			Point p = e.getPoint();
			xMouseDragging = p.x;
			yMouseDragging = p.y;
			repaint();
		} catch (Exception excp) {
			logger.error("Exception: ", excp);
			synchronized (statusLabel) {
				statusLabel.setText(excp.getMessage());
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {

		Point p = e.getPoint();
		Dimension size = getSize();
		float fieldWidth = size.width / Board.BoardSize;
		float fieldHeight = size.height / Board.BoardSize;
		int idx = (int) (p.x / fieldWidth);
		int idy = (int) ((size.height - 1 - p.y) / fieldHeight);
		if (idx >= Board.BoardSize || idy >= Board.BoardSize)
			return;
		ChessClient.chessFrame.setTitle("(" + p.x + "," + p.y + "), (" + idx
				+ "," + idy + "), color: " + game.getColor(idx, idy)
				+ ", item: " + game.GetItem(idx, idy));
	}

	@Override
	public void mousePressed(MouseEvent e) {
		try {
			Player player = game.getPlayer(current);
			if (player instanceof ComputerPlayer)
				return;
			// if (locked)
			// return;
			logger.debug(String.format("Pressed: %s", e.toString()));
			Point p = e.getPoint();
			Dimension size = getSize();
			float fieldWidth = size.width / Board.BoardSize;
			float fieldHeight = size.height / Board.BoardSize;

			logger.debug(String
					.format("Field (%f,%f)", fieldWidth, fieldHeight));
			int currentX = (!reversed) ? p.x : size.width - 1 - p.x;
			int currentY = (!reversed) ? size.height - 1 - p.y : p.y;
			int idx = (int) (currentX / fieldWidth);
			int idy = (int) (currentY / fieldHeight);
			logger.debug(String.format("Elem: (%d,%d)", idx, idy));
			xItemMoving = idx;
			yItemMoving = idy;
			logger.debug(String.format("Gracz chce wziąć element (%d,%d)",
					xItemMoving, yItemMoving));
			GameItem item = game.GetItem(xItemMoving, yItemMoving);
			PlayerType type = game.getPlayer(current).type;
			logger.info("Type: " + type);
			if (!GameItem.colors.get(item).equals(type)) {
				logger.info("Gracz przesuwa nie swoje rzeczy: "
						+ item.toString());
				synchronized (statusLabel) {
					statusLabel.setText(String.format(
							"To nie twój pionek: (%d,%d)", idx + 1, idy + 1));
					xItemMoving = -1;
					yItemMoving = -1;
				}
				// JOptionPane.showMessageDialog(getRootPane(),
				// "To nie twoje pionki");
				return;
			}

			xMouseDragging = e.getPoint().x;
			yMouseDragging = e.getPoint().y;

			xMouseOffset = ((!reversed) ? xItemMoving : Board.BoardSize - 1
					- xItemMoving)
					* fieldWidth - xMouseDragging;
			yMouseOffset = ((reversed) ? yItemMoving : Board.BoardSize - 1
					- yItemMoving)
					* fieldHeight - yMouseDragging;
			repaint();
		} catch (Exception excp) {
			logger.error("Exception: ", excp);
			synchronized (statusLabel) {
				statusLabel.setText(excp.getMessage());
			}
		}

		logger.debug("Finished handling mouse released");
	}

	@Override
	public void mouseReleased(MouseEvent e) {

		try {
			logger.debug(String.format("Mouse released : %s", e.toString()));
			Player player = game.getPlayer(current);
			if (player instanceof ComputerPlayer)
				return;

			if (xItemMoving < 0 || yItemMoving < 0)
				return;

			// if (locked)
			// return;
			Dimension size = getSize();

			final int fieldWidth = size.width / Board.BoardSize;
			final int fieldHeight = size.height / Board.BoardSize;

			Point p = e.getPoint();

			int currentX = (!reversed) ? p.x : size.width - 1 - p.x;
			int currentY = (!reversed) ? size.height - 1 - p.y : p.y;

			int xNewField = currentX / fieldWidth;
			int yNewField = currentY / fieldHeight;

			executeHuman(fieldWidth, fieldHeight, xNewField, yNewField);

		} catch (ChessClientException excp) {

			logger.warn("Chess client exception: ", excp);
			synchronized (statusLabel) {
				statusLabel.setText(excp.getMessage());
			}

		} catch (Exception excp) {
			logger.error("Exception: ", excp);
			synchronized (statusLabel) {
				statusLabel.setText(excp.getMessage());
			}
		}

		// zwolnienie rzeczy
		xItemMoving = -1;
		yItemMoving = -1;
		xMouseDragging = -1;
		yMouseDragging = -1;

		repaint();
	}

	private GameMove lastKilling = null;
	public boolean reversed;

	private void executeHuman(final int fieldWidth, final int fieldHeight,
			int xNewField, int yNewField)
			throws BoardStateModificationException {
		// game.PutItem(xNewField, yNewField, item);
		// game.PutItem(xItemMoving, yItemMoving, GameItem.EMPTY);
		GameMove move = new GameMove(new Point(xItemMoving, yItemMoving),
				new Point(xNewField, yNewField), 0);
		game.checkLegal(move);
		game.perform(move);
		logger.info(String.format("Human player made %dth move: '%s' ",
				++moveNr, move));

		move.color = game.getPlayer(current).type;
		move.id = moveNr;

		history.addElement(move);

		// jeżeli mam wielokrotne bicie, nie uruchamia go
		ArrayList<GameMove> potentialKillings = game.generateKillings(move);
		logger.info("Ruch: " + move + ", bicia: " + potentialKillings
				+ ", wymiana: " + move.exchanged);
		if (move.type.isKilling() && potentialKillings.size() > 0
				&& !move.exchanged) {
			lastKilling = move;
			logger.info("Ruch" + move + ", kolejne bicia: " + potentialKillings);
		} else {
			lastKilling = null;
			logger.info("Next executed");
			if (automatic)
				executeNext();
		}

	}

	private void executeHumanMove() {
		// locked = false;
		logger.info("Human launch " + current);
	}

	private void executeComputer(final Player computer) {
		if (computer == null)
			return;
		logger.info("uruchamiam następnego" + computer);
		Thread t = new Thread(new Runnable() {
			ArrayList<GameMove> killings = new ArrayList<GameMove>();

			@Override
			public void run() {
				boolean interrupted = false;
				try {
					// locked = true;
					GameMove move = null;
					int loop = 0;
					do {
						long from = System.nanoTime();
						synchronized (statusLabel) {
							statusLabel.setText(computer.toString()
									+ " is thinking");
						}
						logger.debug("Computer started thinking");
						move = computer.performMove(game, lastKilling);
						logger.debug("Computer finished thinking");
						if (move == null) {
							synchronized (ChessClient.threads) {
								ChessClient.threads.remove(Thread.currentThread());
								ChessClient.threads.notifyAll();
								logger.debug("All threads: " + ChessClient.threads);
							}									
							logger.info("Przegrał " + computer.toString());
							JOptionPane.showMessageDialog(getRootPane(),
									"Przegrał " + computer.toString());
							// locked = false;					
							repaint();
							return;
						} else {
							xItemMoving = move.from.x;
							yItemMoving = move.from.y;

							final float fps = 25.0f;
							final long sleepMs = (long) (1000.0f / fps);

							System.out.println("From: " + move.from + ", to: "
									+ move.to);

							logger.debug("Computer started animation");

							for (int i = 0; i < fps; i++) {
								if (Thread.currentThread().isInterrupted())
									throw new InterruptedException();
								Dimension d = getSize();
								int fieldWidth = d.width / Board.BoardSize;
								int fieldHeight = d.height / Board.BoardSize;

								int fromX = (!reversed) ? move.from.x
										* fieldWidth
										: (Board.BoardSize - 1 - move.from.x)
												* fieldWidth;
								int fromY = (!reversed) ? (Board.BoardSize - 1 - move.from.y)
										* fieldHeight
										: move.from.y * fieldHeight;

								int toX = (!reversed) ? move.to.x * fieldWidth
										: (Board.BoardSize - 1 - move.to.x)
												* fieldWidth;
								int toY = (!reversed) ? (Board.BoardSize - 1 - move.to.y)
										* fieldHeight
										: move.to.y * fieldHeight;

								Point fromRendering = new Point(fromX, fromY);
								Point toRendering = new Point(toX, toY);
								// System.out.println("From: " +
								// fromRendering +
								// ", to: " + toRendering);

								float OffsetX = (toRendering.x - fromRendering.x)
										/ fps;
								float OffsetY = (toRendering.y - fromRendering.y)
										/ fps;

								xMouseDragging = fromRendering.x + OffsetX * i;
								yMouseDragging = fromRendering.y + OffsetY * i;
								// System.out.println("Current: " +
								// xMouseDragging + ", " +
								// yMouseDragging);
								xMouseOffset = 0;
								yMouseOffset = 0;
								repaint();
								try {
									Thread.sleep(sleepMs);
								} catch (InterruptedException e) {
									throw e;
								}

								xMouseDragging = toRendering.x;
								yMouseDragging = toRendering.y;
								repaint();

							}
							long diff = System.nanoTime() - from;
							logger.debug(String.format(computer.toString()
									+ " finished animation time: %.2f",
									diff / 1e9));

						}

						// JOptionPane.showMessageDialog(getRootPane(),
						// ComputerPlayer.heuristicFunction(game,
						// PlayerType.WHITE));

						logger.info("Loop: " + loop++);

						game.perform(move);
						logger.info(String.format(
								"Computer player made %dth move: '%s' ",
								++moveNr, move));

						move.id = moveNr;
						move.color = computer.type;
						history.addElement(move);

						// logger.debug("Oszacowanie: "
						// + Board.heuristicDistanceFromExchange(
						// game, PlayerType.BLACK));
						killings = game.generateKillings(move);
						logger.info("Ruch: " + move + ", bicia: " + killings
								+ ", wymiana: " + move.exchanged);
						lastKilling = move;
					} while (move.type.isKilling() && killings.size() > 0
							&& !move.exchanged);
					synchronized (statusLabel) {
						statusLabel.setText("Ready");
					}
					lastKilling = null;
					System.out.println("Finished");

					Player next = game.getPlayer((current + 1) % 2);

					if (game.generateMoves(next.type).size() <= 0) {
						JOptionPane.showMessageDialog(getRootPane(),
								"Przegrał " + next.toString());
						logger.info("Przegrał: " + next.toString());
						repaint();
						// locked = false;
						return;
					}

					repaint();
				} catch (BoardStateModificationException excp) {
					logger.warn("Board Exception: ", excp);
				} catch (InterruptedException excp) {
					logger.error("INTERRUPTED: ", excp);
					interrupted = true;
				}

				if (automatic && !interrupted)
					executeNext();

				synchronized (ChessClient.threads) {
					ChessClient.threads.remove(Thread.currentThread());
					ChessClient.threads.notifyAll();
				}

			}
		});

		synchronized (ChessClient.threads) {
			ChessClient.threads.add(t);
		}

		t.start();
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.GRAY);
		Dimension size = getSize();
		Rectangle rect = new Rectangle(size);
		g.fillRect(rect.x, rect.y, rect.width, rect.height);
		Graphics2D g2 = (Graphics2D) g;
		int fieldWidth = rect.width / Board.BoardSize;
		int fieldHeight = rect.height / Board.BoardSize;

		for (int i = 0; i < Board.BoardSize; i++)
			for (int j = 0; j < Board.BoardSize; j++) {
				FieldType type = game.getColor(j, i);
				if (type == FieldType.BLACK)
					g.setColor(Color.BLACK);
				else
					g.setColor(Color.WHITE);

				int yCurrent = (!reversed) ? Board.BoardSize - i - 1 : i;
				int xCurrent = (!reversed) ? j : Board.BoardSize - j - 1;
				g.fillRect(xCurrent * fieldWidth, yCurrent * fieldHeight,
						fieldWidth, fieldHeight);
				// narysowaliśmy sobie pole, teraz trzeba obiekt

				GameItem item;
				item = game.GetItem(j, i);

				int posX = xCurrent * fieldWidth;
				int posY = yCurrent * fieldHeight;

				if (j == xItemMoving && i == yItemMoving)
					continue;

				drawItem(g2, fieldWidth, fieldHeight, item, posX, posY);

			}

		// redraw active

		if (xItemMoving >= 0 && yItemMoving >= 0) {
			GameItem item;
			item = game.GetItem(xItemMoving, yItemMoving);
			drawItem(g2, fieldWidth, fieldHeight, item,
					(int) (xMouseDragging + xMouseOffset),
					(int) (yMouseDragging + yMouseOffset));
		}

	}

	public void setGame(Game g) {
		game = g;
		this.xItemMoving = -1;
		this.xMouseDragging = -1;
		this.xMouseOffset = -1;
		this.yItemMoving = -1;
		this.yMouseDragging = -1;
		this.yMouseOffset = -1;
		this.moveNr = 0;
		this.current = 0;

	}

	public void reset(Player p1, Player p2) {
		game = new Game(p1, p2);
		xItemMoving = -1;
		yItemMoving = -1;
		xMouseDragging = -1;
		yMouseDragging = -1;
		xMouseOffset = -1;
		yMouseOffset = -1;

		current = 0;
		moveNr = 0;

	}

	public boolean isAutomatic() {
		return automatic;
	}

	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}

}
