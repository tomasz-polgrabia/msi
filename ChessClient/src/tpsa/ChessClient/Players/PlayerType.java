package tpsa.ChessClient.Players;

public enum PlayerType {
	BLACK, NONE, WHITE; // dla empty z GameItem

	public int exchangeLine() {
		if (equals(WHITE))
			return 7;
		else
			return 0;
	}

	public int getDirectionMove() {
		switch (this) {
		case WHITE:
			return 1;
		case BLACK:
			return -1;
		default:
			throw new RuntimeException("Unimplemented");
		}
	}

	public PlayerType next() {
		if (equals(WHITE))
			return BLACK;
		else
			return WHITE;
	}

}
