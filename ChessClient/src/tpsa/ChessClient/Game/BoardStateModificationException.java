package tpsa.ChessClient.Game;

import tpsa.ChessClient.ChessClientException;

public class BoardStateModificationException extends ChessClientException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5296701193560599046L;

	public BoardStateModificationException() {
		super();
	}

	public BoardStateModificationException(String msg) {
		super(msg);
	}

}
