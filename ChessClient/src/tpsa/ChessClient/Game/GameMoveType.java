package tpsa.ChessClient.Game;

public enum GameMoveType {
	KILLING_BACK, KILLING_FRONT, KILLING_QUEEN, NONE, NORMAL;

	public boolean isKilling() {
		return equals(KILLING_BACK) || equals(KILLING_FRONT)
				|| equals(KILLING_QUEEN);
	}

}
