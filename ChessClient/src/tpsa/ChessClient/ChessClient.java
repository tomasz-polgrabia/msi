package tpsa.ChessClient;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tpsa.ChessClient.Frames.MainFrame;
import tpsa.ChessClient.Game.Pool;

public class ChessClient {

	/**
	 * @param args
	 */

	public static MainFrame chessFrame;
	public static Pool pool = new Pool(10000);
	public static int depthComputer = 6;
	public static ArrayList<Thread> threads = new ArrayList<Thread>();
	private static Logger logger = Logger.getLogger(ChessClient.class);
	
	static
	{
		logger.setLevel(Level.DEBUG);
	}

	public static void main(String[] args) {
		
		BasicConfigurator.configure();
		
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		    	logger.debug("Available look and feel " + info.getName());
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}		
		
		logger.info("Creating window");
		chessFrame = new MainFrame("Frame");
		chessFrame.setPreferredSize(new Dimension(800, 600));
		chessFrame.pack();
		chessFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		logger.info("Showing window");
		chessFrame.setVisible(true);
		logger.info("Program finished");
	}

}
