package tpsa.ChessClient.Players;

public class NoExchangePromoterPlayer extends ComputerPlayer {

	public NoExchangePromoterPlayer(PlayerType color, String name) {
		super(color, name);
		exchangeWage = 0;
	}

	public NoExchangePromoterPlayer(PlayerType color, String name, int depth) {
		super(color, name, depth);
		exchangeWage = 0;
	}

}
