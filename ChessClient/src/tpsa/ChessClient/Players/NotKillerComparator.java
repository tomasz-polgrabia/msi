package tpsa.ChessClient.Players;

import java.util.Comparator;

import tpsa.ChessClient.Game.GameMove;

public class NotKillerComparator implements Comparator<GameMove> {

	public NotKillerComparator() {
	}

	@Override
	public int compare(GameMove o1, GameMove o2) {
		if (o2.type.equals(o1.type))
		{
			if (o2.value == o1.value)
				return 0;
			if (o2.value > o1.value)
				return -1;
			return 1;
		}
		
		if (o2.type.isKilling() && !o1.type.isKilling())
			return -1;
		
		return 1;
	}

}
