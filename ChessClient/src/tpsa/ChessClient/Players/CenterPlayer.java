package tpsa.ChessClient.Players;

public class CenterPlayer extends ComputerPlayer {

	// z KillerPlayer wyszło, że ogólnie przegrywa z nim (Biały i czarny) dla 8vs8
	
	public CenterPlayer(PlayerType color, String name) {
		super(color, name);
		centerWage = 0.3;
	}

	public CenterPlayer(PlayerType color, String name, int depth) {
		super(color, name, depth);
		centerWage = 0.3;
	}

}
