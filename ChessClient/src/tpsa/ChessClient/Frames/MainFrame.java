package tpsa.ChessClient.Frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.management.RuntimeErrorException;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.log4j.Logger;

import tpsa.ChessClient.ChessClient;
import tpsa.ChessClient.Game.GameMove;
import tpsa.ChessClient.Panels.ChessPanel;
import tpsa.ChessClient.Players.HumanPlayer;
import tpsa.ChessClient.Players.KillerPlayer;
import tpsa.ChessClient.Players.Player;
import tpsa.ChessClient.Players.PlayerType;

public class MainFrame extends JFrame implements ListDataListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 531940984438989051L;
	private static Logger logger = Logger.getLogger(MainFrame.class);
	private JMenuBar menuBar;
	protected Player p1;
	protected Player p2;
	public JLabel statusLabel = new JLabel("Ready");
	private JList ruchy;
	
	

	public MainFrame(String title) throws HeadlessException {
		super(title);

		add(statusLabel, BorderLayout.SOUTH);

		DefaultListModel model = new DefaultListModel();
		ruchy = new JList(model);

		final ChessPanel chessPanel = new ChessPanel(model, statusLabel);
		add(chessPanel);

		// model.addListDataListener(this);

		JScrollPane pane = new JScrollPane(ruchy);

		add(pane, BorderLayout.EAST);
		Dimension d = new Dimension(80, 80);
		pane.setMinimumSize(d);
		pane.setMaximumSize(d);
		pane.setPreferredSize(d);
		pane.setSize(d);

		menuBar = new JMenuBar();
		JMenu gameMenu = new JMenu("Game");

//		p2 = GetPlayer(PlayerType.BLACK, 1, 8, "Terminator");
		p1 = GetPlayer(PlayerType.BLACK, 0, 8, "Obcy");
		
//		p1 = new KillerPlayer(PlayerType.WHITE, "Obcy", 8);
		p2 = GetPlayer(PlayerType.BLACK, 1, 8, "Obcy");

		JMenuItem newGame = new JMenuItem(new AbstractAction("New game") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 194426936972244092L;

			@Override
			public void actionPerformed(ActionEvent arg0) {

				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {

						synchronized (ChessClient.threads) {

							if (ChessClient.threads.size() > 0) {

								logger.info("Waiting for : "
										+ ChessClient.threads);

								for (Thread t : ChessClient.threads) {
									logger.info("Interrupted : " + t);
									t.interrupt();
								}

								try {
									ChessClient.threads.wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}

						}

						Thread.interrupted();

						DefaultListModel model = new DefaultListModel();
						ruchy.setModel(model);
						chessPanel.setHistory(model);
						chessPanel.reset(p1, p2);
						chessPanel.repaint();
						chessPanel.executeCurrent();

						logger.info("New game started");

					}
				});

				t.start();

			}
		});

		gameMenu.add(newGame);
		gameMenu.add(new JCheckBoxMenuItem(new AbstractAction("Automatic") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 3307391287732992845L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean last = chessPanel.isAutomatic();
				chessPanel.setAutomatic(!last);

				logger.info("Changed automatic var to: " + !last);

			}
		}));

		gameMenu.add(new JMenuItem(new AbstractAction("Next move") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 7440775195267201439L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				chessPanel.executeNext();
			}
		}));

		gameMenu.add(new JMenuItem(new AbstractAction("Settings") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 3307391287732992845L;

			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsFrame frame = new SettingsFrame(p1,p2);
				frame.setVisible(true);

				int WhiteType = frame.getTypeFirst();
				int WhiteDepth = frame.getDepthFirst();
				int BlackType = frame.getTypeSecond();
				int BlackDepth = frame.getDepthSecond();
				String WhiteName = frame.getNameFirst();
				String BlackName = frame.getNameSecond();

				p1 = GetPlayer(PlayerType.WHITE, WhiteType, WhiteDepth, WhiteName);
				p2 = GetPlayer(PlayerType.BLACK, BlackType, BlackDepth, BlackName);

				logger.info("Utworzono graczy " + p1 + ", " + p2);

				DefaultListModel model = new DefaultListModel();
				ruchy.setModel(model);
				;
				chessPanel.setHistory(model);
				chessPanel.reset(p1, p2);
				chessPanel.repaint();

			}
		}));

		gameMenu.add(new JMenuItem(new AbstractAction("Save game as") {

			/**
			 * 
			 */
			private static final long serialVersionUID = -6952695841740248969L;

			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] moves = chessPanel.getHistory().toArray();

				try {

					JFileChooser chooser = new JFileChooser(".");
					if (chooser.showSaveDialog(getRootPane()) != JFileChooser.APPROVE_OPTION)
						return;
					
					String fileName = chooser.getSelectedFile().getAbsolutePath();

					FileOutputStream fs = new FileOutputStream(fileName,
							false);

					OutputStreamWriter os = new OutputStreamWriter(fs, "UTF-8");
					Calendar cal = new GregorianCalendar();

					os.write(String.format("[Date \"%s\"]\n", cal.getTime()));
					os.write(String.format("[White \"%s\"]\n", p1.toString()));
					os.write(String.format("[Black \"%s\"]\n", p2.toString()));

					for (Object m : moves) {
						GameMove move = (GameMove) m;
						os.write(move.toString());
						os.write(' ');
					}

					os.close();

				} catch (FileNotFoundException excp) {
					logger.error("Saveing party", excp);
					JOptionPane.showMessageDialog(getRootPane(),
							excp.getMessage(), "Save failed",
							JOptionPane.ERROR_MESSAGE);
				} catch (UnsupportedEncodingException excp) {
					logger.error("Unsupported encoding", excp);
					JOptionPane.showMessageDialog(getRootPane(),
							excp.getMessage(), "Unsupported encoding",
							JOptionPane.ERROR_MESSAGE);
				} catch (IOException excp) {
					logger.error("I/O exception", excp);
					JOptionPane.showMessageDialog(getRootPane(),
							excp.getMessage(), "I/O exception",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		}));

		menuBar.add(gameMenu);
		
		JMenu view = new JMenu("View");
		
		view.add(new JMenuItem(new AbstractAction("Reversed") {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 3868773475691438634L;

			@Override
			public void actionPerformed(ActionEvent e) {
				chessPanel.reversed = !chessPanel.reversed;
				chessPanel.repaint();
				
			}
		}));
		
		menuBar.add(view);

		setJMenuBar(menuBar);

	}

	protected Player GetPlayer(PlayerType color, int whiteType, int whiteDepth, String name) {
		switch (whiteType) {
		case 0:
			return new HumanPlayer(color, name);
		case 1:
			return new KillerPlayer(color, name, whiteDepth);
		default:
			throw new RuntimeErrorException(null, "Type not supported");
		}

	}

	@Override
	public void intervalAdded(ListDataEvent e) {
		// TODO Auto-generated method stub
		// JOptionPane.showMessageDialog(getRootPane(), "JO");
		ruchy.revalidate();
		ruchy.repaint();

	}

	@Override
	public void intervalRemoved(ListDataEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contentsChanged(ListDataEvent e) {
		// TODO Auto-generated method stub

	}

}
